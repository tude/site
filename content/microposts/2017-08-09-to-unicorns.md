---
title: These rare career moments
author: Stephen M. Tudor
date: '2017-08-09'
categories:
  - meta
draft: false
---

My employer was [acquired this week](https://www.frontlineeducation.com/News/Thoma_Bravo_to_Acquire_Frontline_Education). There has been a measure of reveling and celebrating at the office; morale is very high.

It feels good to be part of an organization that is dominating in its vertical market. I've certainly seen the opposite situation first-hand, whether it was due to market forces or a less-than-stellar acquisition.

Without the wounds those experiences left me with, I wouldn't be able to savor this moment nearly as much. It's not often that this happens. A handful times in a career, I'd guess.
