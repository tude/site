+++
author = "Stephen M. Tudor"
categories = ["meta"]
date = "2017-09-11T09:49:02-04:00"
title = "Equifax Hack - What to Do?"
description = "What to do about your newly-compromised identity"
tags = ["equifax"]
+++

Embattled credit reporting agency Equifax, through a security flaw in its
website, has exposed virtually the entire population of US credit-holders
([approximately 143 million people]) to identity fraud.

As far as I can tell, the only thing we can do to protect ourselves is to put
a freeze on our credit with each of five credit reporting agencies (including
the scum of the earth, Equifax). This will prevent anyone (including ourselves)
from doing credit checks or opening new lines of credit in our names. 

Here are places you'll need to visit to take care of this nonsense. For each
one, make sure to securely save your PIN and/or account information, because
that will be necessary to unfreeze your credit in the future.

- [Equifax] - No account required. They will generate a PIN for you.
- [TransUnion] - **Account is required.** You can set your own PIN.
- [Experian] - No account required. You can set your own PIN.
- [Innovis] - No account required. A PIN is mailed to you.
- [ChexSystems] - No account required. A PIN is mailed to you.

[approximately 143 million people]: http://www.npr.org/2017/09/08/549549935/equifax-breach-exposes-personal-data-of-143-million-people
[Equifax]: https://www.freeze.equifax.com/Freeze/jsp/SFF_PersonalIDInfo.jsp
[TransUnion]: https://www.transunion.com/credit-freeze/place-credit-freeze
[Experian]: https://www.experian.com/freeze/center.html
[Innovis]: https://www.innovis.com/personal/securityFreeze
[ChexSystems]: https://www.chexsystems.com/web/chexsystems/consumerdebit/page/securityfreeze/placefreeze
