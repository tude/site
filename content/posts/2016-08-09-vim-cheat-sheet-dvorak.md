+++
author = "Stephen M. Tudor"
categories = ["code"]
date = "2016-08-09T21:04:50-04:00"
description = ""
title = "Vim Cheat Sheet for Programmers: Dvorak Edition"
tags = ["vim"]
aliases = [
  "/2016/08/09/vim-cheat-sheet-dvorak/"
]
+++

I found [this intense Vim cheat sheet], and noticed that the author, Michael, had made the Excel 2011 sources available. Furthermore, he was kind enough to give it a [Copyleft] license.

Being a user of the [Dvorak keyboard layout], I took a few moments tonight to adapt the 300dpi color version to Dvorak. I'm making them available to download them for free, just like the originals. Other than the key layout changes, I bumped the version from 2.0 to 2.1, and updated the dates.

### Download it here: [PDF], [Excel]

<br>

![](/images/vim_cheat_sheet_for_programmers_print_DV_388.png)

I don't have any immediate plans to adapt the original low-res, monochrome, or accessible versions of the cheat sheet, but I'm more than happy to accept contributions from anyone, and make them available here.

[this intense Vim cheat sheet]: http://michael.peopleofhonoronly.com/vim/
[Copyleft]: https://en.wikipedia.org/wiki/Copyleft
[Dvorak keyboard layout]: https://en.wikipedia.org/wiki/Dvorak_Simplified_Keyboard
[PDF]: /files/vim_cheat_sheet_for_programmers_print_DV.pdf
[Excel]: /files/vim_cheat_sheet_for_programmers_print_DV.xlsx
