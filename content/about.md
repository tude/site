Stephen M. Tudor lives near Philadelphia, Pennsylvania in an idyllic post-war Cape. He sometimes succumbs to the urge to write down his thoughts.

Top of mind: _privacy, education, theology/philosophy, code, Unix._
